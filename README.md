# sq

Package sq provides a modern wrapper around database/sql.

Essentially all it does is get rid of names that end with Context. It isn't meant to simplify using a database. See projects like [sqlx](https://pkg.go.dev/github.com/jmoiron/sqlx), [sqlc](https://pkg.go.dev/github.com/kyleconroy/sqlc), [squirrel](https://pkg.go.dev/github.com/Masterminds/squirrel), or [xo](https://pkg.go.dev/github.com/xo/xo) for that.

Aside from the removal of some methods for advanced usage and the addition of an interface encapsulating methods common to DB, Tx, and Conn, the wrapper is as thin as possible. No function contains more than two lines of code. There are no tests because there is no code to test.
