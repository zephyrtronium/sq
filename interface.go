/*
Copyright 2022 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sq

import "context"

// Interface is an interface containing methods common to DB, Tx, and Conn.
type Interface interface {
	Exec(ctx context.Context, query string, args ...any) (Result, error)
	Prepare(ctx context.Context, query string) (*Stmt, error)
	Query(ctx context.Context, query string, args ...any) (*Rows, error)
	QueryRow(ctx context.Context, query string, args ...any) *Row
}

var _, _, _ Interface = (*DB)(nil), (*Tx)(nil), (*Conn)(nil)
