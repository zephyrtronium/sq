/*
Copyright 2022 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package sq provides a modern wrapper around database/sql.
//
// All it does is get rid of names that end with Context. It isn't meant to
// simplify using a database. See sqlx, sqlc, squirrel, or xo for that.
//
// sq defines types which are either aliases to or thin wrappers around types
// from database/sql. These are always convertible to the types they wrap,
// allowing direct interoperation with code that expects the standard library
// types. Additionally, some methods for advanced usage are not wrapped;
// conversion restores access to such methods.
//
// sq makes one addition over database/sql, which is the interface type
// Interface encapsulating methods common to DB, TX, and Conn. Otherwise, it is
// as thin as possible: no function contains more than two lines of code.
package sq

import (
	"context"
	"database/sql"
)

// Conn is a thin wrapper around sql.Conn
type Conn sql.Conn

func (c *Conn) Begin(ctx context.Context, opts *TxOptions) (*Tx, error) {
	tx, err := (*sql.Conn)(c).BeginTx(ctx, opts)
	return (*Tx)(tx), err
}

func (c *Conn) Close() error {
	return (*sql.Conn)(c).Close()
}

func (c *Conn) Exec(ctx context.Context, query string, args ...any) (Result, error) {
	return (*sql.Conn)(c).ExecContext(ctx, query, args...)
}

func (c *Conn) Ping(ctx context.Context) error {
	return (*sql.Conn)(c).PingContext(ctx)
}

func (c *Conn) Prepare(ctx context.Context, query string) (*Stmt, error) {
	stmt, err := (*sql.Conn)(c).PrepareContext(ctx, query)
	return (*Stmt)(stmt), err
}

func (c *Conn) Query(ctx context.Context, query string, args ...any) (*Rows, error) {
	rows, err := (*sql.Conn)(c).QueryContext(ctx, query, args...)
	return (*Rows)(rows), err
}

func (c *Conn) QueryRow(ctx context.Context, query string, args ...any) *Row {
	return (*Row)((*sql.Conn)(c).QueryRowContext(ctx, query, args...))
}

// DB is a thin wrapper around sql.DB.
type DB sql.DB

func Open(driver, dsn string) (*DB, error) {
	db, err := sql.Open(driver, dsn)
	return (*DB)(db), err
}

func (db *DB) Begin(ctx context.Context, opts *TxOptions) (*Tx, error) {
	tx, err := (*sql.DB)(db).BeginTx(ctx, opts)
	return (*Tx)(tx), err
}

func (db *DB) Close() error {
	return (*sql.DB)(db).Close()
}

func (db *DB) Conn(ctx context.Context) (*Conn, error) {
	conn, err := (*sql.DB)(db).Conn(ctx)
	return (*Conn)(conn), err
}

func (db *DB) Exec(ctx context.Context, query string, args ...any) (Result, error) {
	return (*sql.DB)(db).ExecContext(ctx, query, args...)
}

func (db *DB) Ping(ctx context.Context) error {
	return (*sql.DB)(db).PingContext(ctx)
}

func (db *DB) Prepare(ctx context.Context, query string) (*Stmt, error) {
	stmt, err := (*sql.DB)(db).PrepareContext(ctx, query)
	return (*Stmt)(stmt), err
}

func (db *DB) Query(ctx context.Context, query string, args ...any) (*Rows, error) {
	rows, err := (*sql.DB)(db).QueryContext(ctx, query, args...)
	return (*Rows)(rows), err
}

func (db *DB) QueryRow(ctx context.Context, query string, args ...any) *Row {
	return (*Row)((*sql.DB)(db).QueryRowContext(ctx, query, args...))
}

func (db *DB) Stats() DBStats {
	return (*sql.DB)(db).Stats()
}

// Row is a thin wrapper around sql.Row.
type Row sql.Row

func (r *Row) Err() error {
	return (*sql.Row)(r).Err()
}

func (r *Row) Scan(dest ...any) error {
	return (*sql.Row)(r).Scan(dest...)
}

// Rows is a thin wrapper around sql.Rows.
type Rows sql.Rows

func (r *Rows) Close() error {
	return (*sql.Rows)(r).Close()
}

func (r *Rows) ColumnTypes() ([]*ColumnType, error) {
	return (*sql.Rows)(r).ColumnTypes()
}

func (r *Rows) Columns() ([]string, error) {
	return (*sql.Rows)(r).Columns()
}

func (r *Rows) Err() error {
	return (*sql.Rows)(r).Err()
}

func (r *Rows) Next() bool {
	return (*sql.Rows)(r).Next()
}

func (r *Rows) NextResultSet() bool {
	return (*sql.Rows)(r).NextResultSet()
}

func (r *Rows) Scan(dest ...any) error {
	return (*sql.Rows)(r).Scan(dest...)
}

// Stmt is a thin wrapper around sql.Stmt.
type Stmt sql.Stmt

func (s *Stmt) Close() error {
	return (*sql.Stmt)(s).Close()
}

func (s *Stmt) Exec(ctx context.Context, args ...any) (Result, error) {
	return (*sql.Stmt)(s).ExecContext(ctx, args...)
}

func (s *Stmt) Query(ctx context.Context, args ...any) (*Rows, error) {
	rows, err := (*sql.Stmt)(s).QueryContext(ctx, args...)
	return (*Rows)(rows), err
}

func (s *Stmt) QueryRow(ctx context.Context, args ...any) *Row {
	return (*Row)((*sql.Stmt)(s).QueryRowContext(ctx, args...))
}

// Tx is a thin wrapper around sql.Tx.
type Tx sql.Tx

func (tx *Tx) Commit() error {
	return (*sql.Tx)(tx).Commit()
}

func (tx *Tx) Exec(ctx context.Context, query string, args ...any) (Result, error) {
	return (*sql.Tx)(tx).ExecContext(ctx, query, args...)
}

func (tx *Tx) Prepare(ctx context.Context, query string) (*Stmt, error) {
	stmt, err := (*sql.Tx)(tx).PrepareContext(ctx, query)
	return (*Stmt)(stmt), err
}

func (tx *Tx) Query(ctx context.Context, query string, args ...any) (*Rows, error) {
	rows, err := (*sql.Tx)(tx).QueryContext(ctx, query, args...)
	return (*Rows)(rows), err
}

func (tx *Tx) QueryRow(ctx context.Context, query string, args ...any) *Row {
	return (*Row)((*sql.Tx)(tx).QueryRowContext(ctx, query, args...))
}

func (tx *Tx) Rollback() error {
	return (*sql.Tx)(tx).Rollback()
}

func (tx *Tx) Stmt(ctx context.Context, stmt *Stmt) *Stmt {
	return (*Stmt)((*sql.Tx)(tx).StmtContext(ctx, (*sql.Stmt)(stmt)))
}

type (
	ColumnType     = sql.ColumnType
	DBStats        = sql.DBStats
	IsolationLevel = sql.IsolationLevel
	NamedArg       = sql.NamedArg
	NullBool       = sql.NullBool
	NullByte       = sql.NullByte
	NullFloat64    = sql.NullFloat64
	NullInt16      = sql.NullInt16
	NullInt32      = sql.NullInt32
	NullInt64      = sql.NullInt64
	NullString     = sql.NullString
	NullTime       = sql.NullTime
	Out            = sql.Out
	RawBytes       = sql.RawBytes
	Result         = sql.Result
	Scanner        = sql.Scanner
	TxOptions      = sql.TxOptions
)

func Drivers() []string {
	return sql.Drivers()
}

var (
	ErrConnDone = sql.ErrConnDone
	ErrNoRows   = sql.ErrNoRows
	ErrTxDone   = sql.ErrTxDone
)
